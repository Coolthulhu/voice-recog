import matplotlib.pyplot as plt
import numpy as np
import samples
import scipy
import python_speech_features
from python_speech_features import hz2mel
import pywt

img_path = "img/"

def plot_acc(history):
    x = history.epoch
    fig = plt.figure(figsize=(6,3))

    loss_plot = fig.add_subplot(121)
    loss_plot.plot(x, history.history['loss'], linewidth=1.0, color="red")
    loss_plot.plot(x, history.history['val_loss'], linewidth=1.0, color="blue")
    loss_plot.xaxis.set_label_text('epoch')
    loss_plot.yaxis.set_label_text('loss')

    acc_plot = fig.add_subplot(122)
    acc_plot.set_ylim((0.0, 1.0))
    acc_plot.plot(x, history.history['acc'], linewidth=1.0, color="red")
    acc_plot.plot(x, history.history['val_acc'], linewidth=1.0, color="blue")
    acc_plot.xaxis.set_label_text('epoch')
    acc_plot.yaxis.set_label_text('accuracy')

    fig.tight_layout()
    return fig


def make_plots():
    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    curplt.plot(np.linspace(-2.5, 2.5, 100), np.real(scipy.signal.morlet(100)))
    curplt.xaxis.set_label_text('t')
    curplt.yaxis.set_label_text('amplitude')
    fig.savefig(img_path + "morlet.png")

    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    curplt.plot(np.linspace(-2.5, 2.5, 100), np.real(scipy.signal.ricker(100, 10)))
    curplt.xaxis.set_label_text('t')
    curplt.yaxis.set_label_text('amplitude')
    fig.savefig(img_path + "ricker.png")

    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    ww = pywt.Wavelet('haar')
    wwf = ww.wavefun(8)
    wave = wwf[0]
    scaling = wwf[1]
    x = np.linspace(0, 1, len(wave))
    curplt.plot(x, wave)
    curplt.plot(x, scaling, 'r--')
    curplt.xaxis.set_label_text('t')
    curplt.yaxis.set_label_text('amplitude')
    fig.savefig(img_path + "haar.png")

    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    ww = pywt.Wavelet('db5')
    wwf = ww.wavefun(2)
    wave = wwf[0]
    scaling = wwf[1]
    x = np.linspace(0, 1, len(wave))
    curplt.plot(x, wave)
    curplt.plot(x, scaling, color='red')
    curplt.xaxis.set_label_text('t')
    curplt.yaxis.set_label_text('amplitude')
    fig.savefig(img_path + "db5.png")

    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    ww = pywt.Wavelet('sym5')
    wwf = ww.wavefun(2)
    wave = wwf[0]
    scaling = wwf[1]
    x = np.linspace(0, 1, len(wave))
    curplt.plot(x, wave)
    curplt.plot(x, scaling, color='red')
    curplt.xaxis.set_label_text('t')
    curplt.yaxis.set_label_text('amplitude')
    fig.savefig(img_path + "sym5.png")

    def sigmoid(x):
        return 1.0 / (1.0 + np.exp(-x))
    x = np.linspace(-6.0, 6.0, 100)
    y = sigmoid(x)
    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    curplt.plot(x, y)
    curplt.xaxis.set_label_text('x')
    curplt.yaxis.set_label_text('logistic')
    fig.savefig(img_path + "sigmoid.png")


    nfilt = 13
    fbs = python_speech_features.get_filterbanks(nfilt=nfilt,nfft=512,samplerate=22050)
    fig = plt.figure(figsize=(10,5))
    curplt = fig.add_subplot(111)
    colors = plt.cm.nipy_spectral(np.linspace(0, 1, nfilt))
    for i in range(nfilt):
        curplt.plot(fbs[i], linewidth=1.0, color=colors[i])
    curplt.xaxis.set_label_text('samples')
    curplt.yaxis.set_label_text('amplitude')
    fig.savefig(img_path + "filterbank.png")

    x = np.linspace(0, 20, 100, dtype=float)
    y = 10 + 5 * np.sin(x / 4)
    sel = np.arange(5, 100, 5)
    xsel = x[sel]
    fig = plt.figure(figsize=(5,5))
    curplt = fig.add_subplot(111)
    curplt.axis([0, 20, 4, 16])
    curplt.xaxis.set_label_text('samples')
    curplt.yaxis.set_label_text('amplitude')
    curplt.set_xticks(np.floor(xsel))
    curplt.set_yticks(np.arange(4, 16))
    curplt.grid(True, alpha=0.5)
    curplt.plot(x, y, linewidth=1.0)
    curplt.plot(xsel, np.round(y[sel]), 'ro')
    fig.savefig(img_path + "lpcm.png")