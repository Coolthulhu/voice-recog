import scipy.io.wavfile as wav
import numpy as np
import python_speech_features
import wavelets
from collections import namedtuple
from keras.utils.np_utils import to_categorical
import math
import scipy
import random
from scipy import signal
from os import walk
import sys

this = sys.modules[__name__]

this.sample_path = "../data"
this.noises = [0.0]
this.validation_percentage = 0.0
this.cached_samples = None
this.sig_len = 7500
this.pad_len = 0
this.pad_num = 1

def clear_cached():
    this.cached_samples = None

def get_samples():
    for (dirpath, dirnames, filenames) in walk(this.sample_path):
        for file in filenames:
            if not file.endswith(".wav"):
                continue
            num = int(file.split('_', 1)[0])
            (rate, sig) = wav.read(dirpath + "/" + file)
            if this.sig_len > 0 and len(sig) > this.sig_len:
                sig = sig[:this.sig_len]
            sig = sig / np.iinfo(sig.dtype).max
            yield (num, rate, sig)

sample_entry = namedtuple("Sample", ['num', 'rate', 'sig', 'is_validation'])
sample_base = namedtuple("Samples", ['train_x', 'train_y', 'val_x', 'val_y', 'num_dict'])
feature_entry = namedtuple("FeatureVector", ['num', 'data', 'is_validation'])

def mult_samples():
    if not this.cached_samples is None:
        return this.cached_samples
    unprocessed = {}
    all_sigs = []
    for (num, rate, sig) in get_samples():
        unprocessed.setdefault(num, []).append((rate, sig))
        all_sigs.append(sig)

    res = []
    for num, values in unprocessed.items():
        num_values = len(values)
        index_val = set(random.sample(range(num_values),
                                      math.ceil(num_values * this.validation_percentage)))

        for j, val in zip(range(len(values)), values):
            is_validation = j in index_val
            rate = val[0]
            # Sample-wise normalization: mean = 0, standard deviation = 1
            sig = val[1]
            sig = sig - np.mean(sig)
            stddev = np.std(sig)
            if abs(stddev) > 0.0001:
                sig = sig / stddev
            if is_validation:
                this_val = sample_entry(num, rate, sig, is_validation)
                res.append(this_val)
            else:
                for i in range(this.pad_num):
                    full_len = this.sig_len + this.pad_len
                    if full_len > len(sig):
                        padded_sig = np.zeros(full_len, dtype=sig.dtype)
                        mid = np.random.randint(0, full_len - len(sig))
                        padded_sig[mid:(mid + len(sig))] = sig
                    else:
                        padded_sig = sig
                    for noise_mag in this.noises:
                        noisy_sig = padded_sig + np.random.normal(0, noise_mag, len(padded_sig))
                        noisy_val = sample_entry(num, rate, noisy_sig, is_validation)
                        res.append(noisy_val)

    this.cached_samples = res
    return this.cached_samples

def dict_to_vectors(num_dict, category_num):
    # Find maximum dimensions
    dims = (0, 0)
    for i, values in num_dict.items():
        for (val, is_validation) in values:
            shape_one = val.shape[1] if len(val.shape) > 1 else 1
            dims = (max(dims[0], val.shape[0]), max(dims[1], shape_one))
    # Reshape all matrices to maximum dimension
    old_num_dict = num_dict
    num_dict = {}
    for i, values in old_num_dict.items():
        new_values = []
        for (curval, is_validation) in values:
            val = curval
            if len(val.shape) == 1:
                val = np.reshape(val, (-1, 1))
            if val.shape[0] < dims[0] or val.shape[1] < dims[1]:
                nval = np.zeros(dims, dtype=val.dtype)
                nval[:val.shape[0], :val.shape[1]] = val
            else:
                nval = val
            new_values.append((nval, is_validation))
        num_dict[i] = new_values
    # Concatenate and label
    training_x = []
    validate_x = []
    training_y = []
    validate_y = []
    for i, values in num_dict.items():
        for pr in values:
            val = pr[0]
            is_validation = pr[1]
            if is_validation:
                validate_x.append(val)
                validate_y.append(to_categorical(i, num_classes=category_num))
            else:
                training_x.append(val)
                training_y.append(to_categorical(i, num_classes=category_num))

    input_x = np.stack(training_x, axis=0)
    input_y = np.stack(training_y, axis=0)

    if len(validate_x) > 0:
        val_x = np.stack(validate_x, axis=0)
        val_y = np.stack(validate_y, axis=0)
        val_x = np.squeeze(val_x)
        val_y = np.squeeze(val_y)
    else:
        val_x = ()
        val_y = ()

    input_x = np.squeeze(input_x)
    input_y = np.squeeze(input_y)
    return sample_base(input_x, input_y, val_x, val_y, num_dict)

def prepare_samples_mfcc_feat(mfcc_len, mfcc_num_per_sample):
    num_dict = {}
    for s in mult_samples():
        mfcc_feat = python_speech_features.mfcc(s.sig, s.rate)
        num_dict.setdefault(s.num, []).append((mfcc_feat, s.is_validation))
    return dict_to_vectors(num_dict, len(num_dict))

def wavelet_and_rescale(data, wavelet, width, n_interp):
    n = len(data)
    length = min(10 * width, n)
    cwt = signal.convolve(data, wavelet(length, width), mode='same')
    return np.interp(np.linspace(0, n, n_interp), np.arange(n), cwt)

def prepare_samples_wavelets(wavelet, samples_per, hop, depth=None, stats=True, waves=False):
    num_dict = {}
    for s in mult_samples():
        wave_feat = wavelets.segmented_wavelet(s.sig, wavelet, samples_per, hop, depth, stats, waves)
        num_dict.setdefault(s.num, []).append((wave_feat, s.is_validation))
    return dict_to_vectors(num_dict, len(num_dict))

def prepare_samples_raw():
    num_dict = {}
    for s in mult_samples():
        num_dict.setdefault(s.num, []).append((s.sig, s.is_validation))
    return dict_to_vectors(num_dict, len(num_dict))

def prepare_samples_dct(n_fft):
    num_dict = {}
    for s in mult_samples():
        sig_fft = scipy.fftpack.dct(s.sig)
        clipped = sig_fft[slice(0, n_fft)]
        if n_fft > clipped.shape[0]:
            clipped = np.pad(clipped, (0, n_fft - clipped.shape[0]), mode='constant')
        clipped = clipped.reshape(1, n_fft)
        num_dict.setdefault(s.num, []).append((clipped, s.is_validation))
    return dict_to_vectors(num_dict, len(num_dict))

def stft(x, rate, frame_time, step_time):
    frame_samples = int(frame_time*rate)
    frame_dist = int(step_time*rate)
    w = scipy.hanning(frame_samples)
    y = scipy.array([scipy.fft(w*x[i:i+frame_samples])
                     for i in range(0, len(x)-frame_samples, frame_dist)])
    return y

def prepare_samples_stft(frame_time, step_time):
    num_dict = {}
    for s in mult_samples():
        sig_stft = stft(s.sig, s.rate, frame_time, step_time)
        sig_stft = np.real(sig_stft)
        num_dict.setdefault(s.num, []).append((sig_stft, s.is_validation))
    return dict_to_vectors(num_dict, len(num_dict))

