import numpy as np
import keras_model
import samples
import plot
from time import time
from keras import callbacks

all_tests = []

def perf_test(sample_list, name, iters=2000):
    n = len(sample_list.num_dict)
    input_shape = sample_list.train_x.shape[1:]
    model = keras_model.get_model(input_shape, n)
    stop_callback = callbacks.EarlyStopping(monitor='val_acc',
                                            min_delta=0.01,
                                            patience=100, verbose=1)
    time_pre = time()
    history = model.fit(sample_list.train_x, sample_list.train_y,
                        epochs=iters, batch_size=128, verbose=1,
                        validation_data=(sample_list.val_x, sample_list.val_y),
                        callbacks=[stop_callback])
    time_post = time()
    print_history_summary(name, history, time_post - time_pre)
    all_tests.append((name, history, time))
    return (history, model)

def print_history_summary(name, history_obj, time):
    history = history_obj.history
    iters = len(history['acc'])
    s = "{} & {:} & {:.0f}\\% & {:.0f}\\% & {:.0f}\\%\\\\ \\hline"
    print(s.format(
            name,
            iters,
            100 * history['acc'][iters - 1],
            100 * history['val_acc'][iters - 1],
            100 * max(history['val_acc'])))

samples.noises = [0.1]
samples.validation_percentage = 0.2
samples.clear_cached()

perf_test(samples.prepare_samples_mfcc_feat(13, 100), "MFCC-13-cepstra")
perf_test(samples.prepare_samples_wavelets('db5', 512, 256, None, True, False), "wavestats-db5-512")
perf_test(samples.prepare_samples_wavelets('haar', 32, 16, None, True, False), "wavestats-haar-32")
perf_test(samples.prepare_samples_wavelets('haar', 128, 64, None, False, True), "wavelet-haar-128")
perf_test(samples.prepare_samples_wavelets('sym5', 64, 32, None, True, True), "wavelet-wavestats-sym5-64")
perf_test(samples.prepare_samples_wavelets('sym5', 1024, 512, None, True, False), "wavestats-sym5-1024")
perf_test(samples.prepare_samples_stft(0.004, 0.002), "STFT, 40ms frames")
perf_test(samples.prepare_samples_raw(), "PCM")
perf_test(samples.prepare_samples_dct(512), "DCT 512 amplitudes")


for tpl in all_tests:
    print_history_summary(tpl[0], tpl[1], tpl[2])
    fig = plot.plot_acc(tpl[1])
    #fig.savefig("img/" + tpl[0] + ".png")