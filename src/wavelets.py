import numpy as np
import scipy
import pywt

def windowed_wavelet(sig, start, end, wavelet, depth, stats, add_waves):
    siglen = end - start
    w = scipy.hanning(siglen)
    if end > len(sig):
        # Pad to length
        newsig = np.zeros(siglen, dtype=sig.dtype)
        newsig[:(len(sig) - start)] = sig[start:]
        sig = newsig
        start = 0
        end = siglen
    y = scipy.array(w*sig[start:end])
    waves = pywt.wavedec(y, wavelet, level=depth)
    energies = [np.sum(np.abs(x)) for x in waves]
    stddevs = [np.std(x) for x in waves]
    res = []
    if add_waves:
        res.append(np.concatenate(waves))
    if stats:
        res.extend([energies, stddevs])
    return np.concatenate(res)

def segmented_wavelet(sig, wavelet_name, samples_per, hop, depth, stats=True, waves=False):
    wavelet = pywt.Wavelet(wavelet_name)
    if depth is None:
        depth = pywt.dwt_max_level(samples_per, wavelet)

    res = []
    for i in range(0, len(sig), hop):
        wave = windowed_wavelet(sig, i, i + samples_per, wavelet, depth, stats, waves)
        res.append(wave)
    return np.stack(res)
