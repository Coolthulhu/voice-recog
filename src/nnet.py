import os
os.environ['THEANO_FLAGS'] = "device=gpu"

import theano
import numpy

from theano import tensor as TT
from collections import OrderedDict

theano.config.on_unused_input='ignore'
theano.config.exception_verbosity='low'
theano.config.optimizer='fast_compile'
theano.config.compute_test_value = 'raise'

class model(object):
    def __init__(self, input_size, hidden_size, output_size):
        self.hidden_size = hidden_size
        self.input_size = input_size
        self.output_size = output_size

        test_size = 100
        # input (where first dimension is time)
        self.inputs = TT.matrix("inputs")
        self.inputs.tag.test_value = numpy.random.rand(test_size, self.input_size)
        # target (where first dimension is time)
        self.targets = TT.matrix("targets")
        self.targets.tag.test_value = numpy.random.rand(1, self.output_size)
        # initial hidden state of the RNN
        self.initial_hidden = TT.vector("initial_hidden")
        self.initial_hidden.tag.test_value = numpy.zeros(shape=(self.hidden_size))
        # learning rate
        self.learning_rate_tensor = TT.scalar("learning_rate")
        self.learning_rate_tensor.tag.test_value = 0.1

        # recurrent weights as a shared variable
        self.recurrent_weights = theano.shared(numpy.random.uniform(size=(self.hidden_size, self.hidden_size), low=-1.0, high=1.0))
        self.recurrent_weights.tag.test_value = numpy.random.uniform(size=(self.hidden_size, self.hidden_size), low=-1.0, high=1.0)
        # input to hidden layer weights
        self.input_weights = theano.shared(numpy.random.uniform(size=(self.input_size, self.hidden_size), low=-1.0, high=1.0))
        self.input_weights.tag.test_value = numpy.random.uniform(size=(self.hidden_size, self.hidden_size), low=-1.0, high=1.0)
        # hidden to output layer weights
        self.output_weights = theano.shared(numpy.random.uniform(size=(self.hidden_size, self.output_size), low=-1.0, high=1.0))
        self.input_weights.tag.test_value = numpy.random.uniform(size=(self.hidden_size, self.hidden_size), low=-1.0, high=1.0)

        # L2 norm used for normalization
        self.L2_sqr = 0
        self.L2_sqr += (self.recurrent_weights ** 2).sum()
        self.L2_sqr += (self.input_weights ** 2).sum()
        self.L2_sqr += (self.output_weights ** 2).sum()

        #self.output_activation = TT.nnet.softmax
        self.output_activation = lambda x: TT.nnet.relu(x)
        #self.output_activation = lambda x: x


        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(input_current, hidden_last, recurrent_weights, input_weights, output_weights):
            hidden_current = TT.tanh(TT.dot(input_current, input_weights) + TT.dot(hidden_last, recurrent_weights))
            #hidden_current = TT.tanh(TT.dot(input_weights, input_current) + TT.dot(recurrent_weights, hidden_last))
            #output_current = TT.dot(hidden_current, output_weights)
            output_current = self.output_activation(TT.dot(hidden_current, output_weights))[0]
            #output_current = TT.nnet.softmax(TT.dot(output_weights, hidden_current))[0]
            return hidden_current, output_current

        # the hidden state `hidden_state` for the entire sequence, and the output for the
        # entrie sequence `output` (first dimension is always time)
        [self.hidden_state, self.output_all], _ = theano.scan(
            step,
            sequences=self.inputs,
            outputs_info=[self.initial_hidden, None],
            non_sequences=[self.recurrent_weights, self.input_weights, self.output_weights])

        #self.output = self.output_all[-1:]
        self.output = self.output_all
        self.error = ((self.output - self.targets) ** 2).sum()

        self.gradient_recurrent, self.gradient_input, self.gradient_output = TT.grad(self.error, [self.recurrent_weights, self.input_weights, self.output_weights])
        self.updates_arg = OrderedDict([(self.recurrent_weights, self.recurrent_weights - self.learning_rate_tensor * self.gradient_recurrent),
                                        (self.input_weights, self.input_weights - self.learning_rate_tensor * self.gradient_input),
                                        (self.output_weights, self.output_weights - self.learning_rate_tensor * self.gradient_output)])

        self.train_one = theano.function([self.initial_hidden, self.inputs, self.targets, self.learning_rate_tensor], self.error, updates = self.updates_arg)
        self.classify = theano.function([self.initial_hidden, self.inputs], self.output)
        self.get_error = theano.function([self.initial_hidden, self.inputs, self.targets], self.error)
        self.prepare_targets = theano.function([self.inputs], self.output_activation(self.inputs))

    def vector_from_indices(vec, size):
        ret = numpy.zeros([len(vec), size])
        for vec_index, i in zip(vec, range(len(vec))):
            ret[i, vec_index] = 1
        return ret

    def learn_one(self, learn_inputs, target_index, learning_rate = 0.01):
        init_h = numpy.zeros(shape=(self.hidden_size))
        out_vec = model.vector_from_indices([target_index], self.output_size)
        learn_targets = self.prepare_targets(out_vec)
        err = self.train_one(init_h, learn_inputs, learn_targets, learning_rate)

        return err
        #print("Inputs:")
        #print(learn_inputs)
        #print("Targets:")
        #print(learn_targets)
        #print("Outputs:")
        #out = self.classify(init_h, learn_inputs)
        #print(out)
        #am = numpy.argmax(out, axis=1)
        #print("Argmax:")
        #print(am)
        #return (learn_inputs, learn_targets, out, am)
    def classify_one(self, in_vec, expected_index = None):
        init_h = numpy.zeros(shape=(self.hidden_size))
        out = self.classify(init_h, in_vec)
        ret = numpy.argmax(out, axis=1)[-1]
        if expected_index != None:
            out_vec = model.vector_from_indices([expected_index], self.output_size)
            targets = self.prepare_targets(out_vec)
            err = self.get_error(init_h, in_vec, targets)
            #print("error: {}, actual: {}".format(err, act_err))
            return [ret, err]
        return [ret]

def test_model(mod, num = 10, num_iters = 10, learning_rate = 0.01):
    init_h = numpy.zeros(shape=(mod.hidden_size))
    #learn_inputs = numpy.random.uniform(size=(num, self.input_size), low=-1.0, high=1.0)
    #out_indices = numpy.random.randint(0, self.output_size, size=num)
    out_indices = range(mod.output_size)
    out_vec = rnn.model.vector_from_indices(out_indices, mod.output_size)
    learn_targets = mod.prepare_targets(out_vec)
    learn_inputs = out_vec

    nextprint = 0
    for i in range(num_iters):
        err = mod.train_one(init_h, learn_inputs, learn_targets, learning_rate)
        if i >= nextprint:
            print(err)
            nextprint += num_iters // 10

    print("Inputs:")
    print(learn_inputs)
    print("Targets:")
    print(learn_targets)
    print("Outputs:")
    out = mod.classify(init_h, learn_inputs)
    print(out)
    am = numpy.argmax(out, axis=0)
    print("Argmax:")
    print(am)
    return (learn_inputs, learn_targets, out, am)