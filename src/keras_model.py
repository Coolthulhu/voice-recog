from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, LSTM, Flatten
from keras import regularizers
from keras.constraints import max_norm
import keras.optimizers

def get_model(input_shape, output_len):
    model = Sequential()
    model.add(Dropout(0.2, input_shape=input_shape))

    model.add(keras.layers.noise.GaussianNoise(0.1))

    if len(input_shape) > 1:
        model.add(Flatten())

    model.add(Dense(128, activation='relu',
                    kernel_constraint=max_norm(10.0)))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu',
                    kernel_constraint=max_norm(10.0)))
    model.add(Dropout(0.2))
    model.add(Dense(output_len, activation='softmax',
                    kernel_constraint=max_norm(10.0)))

    opt = keras.optimizers.adam(lr=0.001, beta_1=0.9, beta_2=0.999,
                                epsilon=1e-08, decay=0.005)
    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])

    return model